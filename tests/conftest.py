import os
from glob import glob

TEST_DIR = "tests"
TEST_EXAMPLES_DIR = os.path.join(TEST_DIR, "examples")
ORACLES_DIR = os.path.join(TEST_DIR, "examples", "oracles")


def pytest_generate_tests(metafunc):
    """
    A special pytest function for advanced parametrization
    """
    parametrixed_fixtures = {
        "examples": _examples_generator,
    }

    for (fixture, generator,) in parametrixed_fixtures.items():
        if fixture in metafunc.fixturenames:
            metafunc.parametrize(
                fixture, generator(metafunc), ids=generator(metafunc, ids=True),
            )


def _examples_generator(metafunc, ids=False):
    """
    Find the harnesses uses in our tests.

    Use all harnesses that are included with the release passed to pytest
    """
    data = []
    for example in glob(os.path.join(TEST_EXAMPLES_DIR, "*.py")):
        data += [(example, os.path.join(ORACLES_DIR, os.path.basename(example)))]

    if ids:
        return [os.path.basename(d[0]) for d in data]
    return data
