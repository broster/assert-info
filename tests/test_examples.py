from assert_info.assert_token import fixed_text


def test_examples(examples):
    test_path, oracle_path = examples
    fixed_output = fixed_text(test_path)

    with open(oracle_path) as oracle_file:
        oracle_output = oracle_file.read()

    assert fixed_output == oracle_output
