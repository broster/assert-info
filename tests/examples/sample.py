"""
This is an example test
"""

CONST = "constant"


def main():
    assert CONST == "a string"
    assert 1 < 3 <= 5
    assert True or False

    if CONST:

        assert bool("a string")
    elif CONST:
        assert bool("a") and bool("b")
    else:
        assert bool(1) or bool(2)

    while False:
        for _ in range(100):
            assert CONST, "some text"
            assert True or False, "some text"
            assert True and False
            assert 1, "more text"
            assert None is not None

        assert 1 < 3
        assert 4 > 5
        assert 2 == 1
        assert 1 != 2

    assert CONST is None
    assert CONST is None, "a message"
    raise AssertionError("a message")
    raise AssertionError()

    assert (lambda x: True)(1)


if __name__ == "__main__":
    main()