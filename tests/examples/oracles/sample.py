from assertions import assert_equal, assert_greater, assert_is, assert_is_not, assert_less, assert_not_equal, assert_true
"""
This is an example test
"""

CONST = "constant"


def main():
    assert_equal(CONST, "a string")
    assert 1 < 3 <= 5
    assert True or False

    if CONST:

        assert_true(bool("a string"))
    elif CONST:
        assert bool("a") and bool("b")
    else:
        assert bool(1) or bool(2)

    while False:
        for _ in range(100):
            assert CONST, "some text"
            assert True or False, "some text"
            assert True and False
            assert 1, "more text"
            assert_is_not(None, None)

        assert_less(1, 3)
        assert_greater(4, 5)
        assert_equal(2, 1)
        assert_not_equal(1, 2)

    assert_is(CONST, None)
    assert CONST is None, "a message"
    raise AssertionError("a message")
    raise AssertionError()

    assert_true((lambda x: True)(1))


if __name__ == "__main__":
    main()

