from hypothesis import given
from hypothesis.strategies import (
    text,
    characters,
    booleans,
    composite,
    integers,
    decimals,
    dictionaries,
    lists,
    randoms,
)
from assert_info.assert_token import fix_text

ASSERT_REPLACEMENTS = {
    "==": "assert_equal",
    "!=": "assert_not_equal",
    "is": "assert_is",
    "is not": "assert_is_not",
    "in": "assert_in",
    "not in": "assert_not_in",
    "<": "assert_less",
    "<=": "assert_less_equal",
    ">": "assert_greater",
    ">=": "assert_greater_equal",
}

strings = text(
    characters(max_codepoint=128, blacklist_categories=("Cc", "Cs")), min_size=1
)


@composite
def functions(draw):
    rand = draw(randoms())
    bool = draw(booleans())
    num = draw(integers())
    text = '"{}"'.format(draw(strings).replace("\\", "\\\\").replace('"', '\\"'))
    kwarg_str = rand.choice(['"string"', "*args", "*args, **kwargs", "**kwargs"])
    return "func({}, {}, {}, {})".format(bool, num, text, kwarg_str)


@composite
def operators(draw):
    idx = draw(integers(min_value=0, max_value=len(ASSERT_REPLACEMENTS) - 1))
    return list(ASSERT_REPLACEMENTS.keys())[idx]


@given(booleans(), booleans(), operators())
def test_bools(arg1, arg2, operator):
    run_checks(arg1, arg2, operator)


@given(decimals(), decimals(), operators())
def test_numbers(arg1, arg2, operator):
    run_checks(arg1, arg2, operator)


@given(
    dictionaries(strings, integers()), dictionaries(integers(), strings), operators()
)
def test_dictionaries(arg1, arg2, operator):
    run_checks(arg1, arg2, operator)


@given(lists(strings), lists(integers()), operators())
def test_lists(arg1, arg2, operator):
    run_checks(arg1, arg2, operator)


@given(functions(), functions(), operators())
def test_lists(arg1, arg2, operator):
    run_checks(arg1, arg2, operator)


@given(strings, strings, operators())
def test_strings(arg1, arg2, operator):
    s1 = '"{}"'.format(arg1.replace("\\", "\\\\").replace('"', '\\"'))
    s2 = '"{}"'.format(arg2.replace("\\", "\\\\").replace('"', '\\"'))
    run_checks(s1, s2, operator)


def run_checks(arg1, arg2, operator):
    _assert_comparison(arg1, operator, arg2)
    _assert_true(arg1)
    _assert_false(arg2)


def _assert_true(arg):
    assert "assert_true({})".format(arg) == _fixed_assert(arg)


def _assert_false(arg):
    assert "assert_false({})".format(arg) == _fixed_assert("not {}".format(arg))


def _assert_comparison(arg1, operator, arg2):
    text = _fixed_assert("{} {} {}".format(arg1, operator, arg2))
    func = ASSERT_REPLACEMENTS[operator]
    assert text == "{}({}, {})".format(func, arg1, arg2)


def _fixed_assert(arg):
    test_string = "assert {}".format(arg)
    text = fix_text(test_string).splitlines()
    if len(text) == 1:
        import ast

        print(test_string, text, ast.dump(ast.parse(test_string)))
    return text[1]
